syntax = "proto3";
package journals;
import "data/item.proto";
option go_package = "gitlab.com/skllzz/jmot/gojmot-client/pb/keeper/journals";

// сервис операций с журналами
// журналы представляют из себя последовательную запись действий над элементами, удобны для синхронизации так как каждый
// элемент записи имеет инкрементально увеличиваемый аттрибут номера изменения и журнал всегда можно проиграть с заданного номера изменений
service Operator {
  // добавление или обновление записи в журнале, возвращает прежнее значение элемента, если оно было
  rpc Append(AppendRequest) returns (data.Item);
  // чтение данных журнала
  rpc Read(ReadRequest) returns (stream data.Item);
  // последнее актуальное значение элемента в журнале (вернет ошибку если элемент не найден)
  rpc Value(ValueRequest) returns (data.Item);
}

// запрос на добавления данных в журнал
message AppendRequest {
  // имя журнала
  string journal_name = 1;
  // добавляемый или обновляемый элемент
  data.Item item = 2;
  // добавлять только в случае если такого элемента нет
  bool if_not_exists=3;
}

// Режим чтения журнала от хвоста к голове
message Forward {
  // минимальный порядковый номер изменения
  int64 seq_no = 1;
  // режим ожидания появление новых данных после получения последнего элемента
  bool continuous = 3;
}

// Режим чтения журнала от головы к хвосту порциями (чтение останавливается по инициативе клиента в момент отмены самого метода)
message Backward {
  // последний известный порядковый номер с хвоста (0 если не задавать)
  // будет отданы элементы с номером меньше заданного (или все если 0)
  int64 seq_no = 1;
}

// Режим чтения последних значений журнала
message Values {
  // диапазон ключей (ключ является конкатенацией {{data.Item.Type}}.{{data.Item.IdType}}
  data.IdRange range = 1;
  // обратный порядок сортировки
  bool descending = 2;
}

// запрос на чтение данных журнала
message ReadRequest {
  // имя журнала
  string journal_name = 1;
  // размер единичной порции элементов, если меньше или равно 0 то используется значение по умолчанию
  int32 page_size = 2;
  // вариант чтения журнала
  oneof Mode {
    // вперед от хвоста к голове
    Forward forward = 3;
    // назад от головы к хвосту
    Backward backward = 4;
    // список последних значений
    Values values = 5;
  }
}

// Запрос чтения единичного элемента журнала
message ValueRequest {
  // имя журнала
  string journal_name = 1;
  // тип элемента
  string type = 2;
  // уникальный идентификатор элемента
  string id = 3;
}
